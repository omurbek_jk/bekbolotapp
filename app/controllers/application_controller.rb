class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :setUser


  private
  def setUser
	 @current_user = current_user
  end
end
