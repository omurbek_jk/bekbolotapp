require 'test_helper'

class MainControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get main_index_url
    assert_response :success
  end

  test "should get users" do
    get main_users_url
    assert_response :success
  end

  test "should get blogs" do
    get main_blogs_url
    assert_response :success
  end

end
